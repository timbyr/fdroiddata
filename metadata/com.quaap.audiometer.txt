Categories:Multimedia
License:GPLv3+
Web Site:http://quaap.com/D/AudioMeter
Source Code:https://github.com/quaap/AudioMeter
Issue Tracker:https://github.com/quaap/AudioMeter/issues

Summary:A simple VU-style audio meter
Description:
A simple VU-style audio meter.  You can choose from several formulas to allow
the meter to work in low- and high-noise level enviromnents.

I noticed there was no audio meter in F-Droid, so I thought I'd build one from
scratch.
.

Repo Type:git
Repo:https://github.com/quaap/AudioMeter

Build:1.0,1
    commit=v1.0.0
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v.0
Update Check Mode:Tags
