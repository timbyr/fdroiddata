Categories:System
License:GPLv3+
Web Site:
Source Code:https://github.com/renyuneyun/CamCov
Issue Tracker:https://github.com/renyuneyun/CamCov/issues

Auto Name:CamCov
Summary:Use your camera as background
Description:
Gain more safety while walking with your eyes on the screen.

Dut to technical issues, this software actually uses the camera as a translucent
covering layer (overlay). However, that behaves quite similar to using it as
background.

Similar software: transparent screen

Interoperable by Broadcast/Intent. See INTEROPERABILITY.md in the source code
for details.
.

Repo Type:git
Repo:https://github.com/renyuneyun/CamCov

Build:0.3,3
    commit=v0.3
    subdir=app
    gradle=yes

Build:0.3.1,4
    commit=v0.3.1
    subdir=app
    gradle=yes

Build:0.4,5
    commit=v0.4
    subdir=app
    gradle=yes

Build:0.4.1,6
    commit=v0.4.1
    subdir=app
    gradle=yes

Build:0.4.2,7
    commit=v0.4.2
    subdir=app
    gradle=yes

Build:0.4.3,8
    commit=v0.4.3
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.4.3
Current Version Code:8
