Categories:Theming
License:GPLv3+
Web Site:
Source Code:https://github.com/enricocid/Battery-Live
Issue Tracker:https://github.com/enricocid/Battery-Live/issues

Auto Name:Battery Live
Summary:Set the wallpaper based on the battery level
Description:
Battery Live is a simple app to apply a livewallpaper based on the battery
level. It has a color chooser dialog where user can choose the color of the
charge from presets or from custom hex code. The color of the discharge level is
the complementary color of the applied color. There are also some UI animations
and a function to determine if the applied color is light or not used to change
the toolbar/status bar color to dark or light. The app supports 5.0+ devices.
.

Repo Type:git
Repo:https://github.com/enricocid/Battery-Live

Build:1.0,1
    commit=1.0
    subdir=project/app
    gradle=yes

Build:2.0,2
    commit=2.0
    subdir=project/app
    gradle=yes

Build:2.0.2,4
    commit=2.0.2
    subdir=project/app
    gradle=yes

Build:2.0.3,5
    commit=2.0.3
    subdir=project/app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:2.0.3
Current Version Code:5
