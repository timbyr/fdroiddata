Categories:Games
License:GPLv3
Web Site:https://secuso.org/pfa
Source Code:https://github.com/SecUSo/privacy-friendly-yahtzee
Issue Tracker:https://github.com/SecUSo/privacy-friendly-yahtzee/issues

Auto Name:Yahtzee Dicer
Summary:Play Yahtzee dice game
Description:
Play the dice game Yahtzee.  It can roll dice up to three rounds and also save
dice for the next round. A small round counter helps to track the number of
rounds. The scorecard which is required to play Yahtzee is not included.

The app belongs to the group of Privacy Friendly Apps developed at the
[https://secuso.org/ SECUSO research group] of Technische Universitaet
Darmstadt. It does not require any permission and relinquishes advertisement and
tracking mechanisms.
.

Repo Type:git
Repo:https://github.com/SecUSo/privacy-friendly-yahtzee

Build:1.0,1
    commit=v1.0
    subdir=app
    gradle=yes

Build:1.1,2
    commit=v1.1
    subdir=app
    gradle=yes

Build:1.1.1,3
    commit=v1.1.1
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.1.1
Current Version Code:3
