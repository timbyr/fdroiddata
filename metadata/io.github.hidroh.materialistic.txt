AntiFeatures:NonFreeNet
Categories:Internet
License:Apache2
Web Site:
Source Code:https://github.com/hidroh/materialistic
Issue Tracker:https://github.com/hidroh/materialistic/issues

Auto Name:Materialistic
Summary:Interact with the "Hacker news" site
Description:
Hacker News client, designed for both phones & tablets, with optimized speed &
network
.

Repo Type:git
Repo:https://github.com/hidroh/materialistic.git

Build:3.0,61
    commit=61
    subdir=app
    gradle=yes

Build:3.1,62
    commit=62
    subdir=app
    gradle=yes

Build:3.1,63
    commit=63
    subdir=app
    gradle=yes

Build:3.1,65
    commit=65
    subdir=app
    gradle=yes

Auto Update Mode:Version %c
Update Check Mode:Tags
Current Version:3.1
Current Version Code:65
